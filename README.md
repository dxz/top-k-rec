# **A Method Collection for Top-k Recommendation**
* * *
* ## **Introduction**

    The collection consists of following methods. Some of them rely on [theano](http://deeplearning.net/software/theano/):

    1. [**Bayesian Personalized Ranking (BPR)**](https://arxiv.org/abs/1205.2618)

         BPR is the very first version of the BPR based methods.

         It is only applicable in in-matrix recommendation scenario.

    2. [**Visual Bayesion Personalized Ranking (VBPR)**](https://arxiv.org/abs/1510.01784)

         VBPR is the extension of BPR to combine visual contents in the rating prediction.

         It can recommend videos in both in-matrix and out-of-matrix recommendation scenarios.

    3. [**DeepMusic (DPM)**](https://papers.nips.cc/paper/5004-deep-content-based-music-recommendation)

         DPM uses multiple layer perceprion (MLP) to learn the content latent vectors from MFCC.

         It recommends videos in both in-matrix and out-of-matrix recommendation scenarios.

    4. [**Collaborative Embedding Regression (CER)**](https://arxiv.org/abs/1612.06935)

         CER leverages linear embedding to learn the latent factors.

         It recommends videos in both in-matrix and out-of-matrix recommendation scenarios.

    Some methods have already released their source codes:

    1. [**Collaborative Topic Regression (CTR)**](http://www.cs.columbia.edu/~blei/papers/WangBlei2011.pdf)

         CTR uses LDA to learn the topic distribution from the textual content vectors, then performs the collaborative regression to learn the user and item latent vectors.

         CTR can perform in-matrix and out-of-matrix recommendation but only with the textual content vectors.

         The source code can be downloaded from [here](http://www.cs.cmu.edu/~chongw/citeulike/).

    2. [**Collaborative Deep Learning (CDL)**](https://arxiv.org/abs/1409.2944)

         CDL uses stacked denoising auto-encoder (SDAE) to learn the content latent vectors, then performs the collaborative regression to learn the user and item latent vectors.

         CDL can perform in-matrix and out-of-matrix recommendation.

         The source code can be downloaded from [here](http://www.wanghao.in/code/cdl-release.rar).

         CDL originally supports textual contents only.

         We can make CDL support non-textual contents by replacing the binary visiable layer with Gaussian visiable layer.

* * *
* ## **Instruction**

    All the codes in the repository are written in Python 2.7.

    To simplify the installation of Python 2.7, the code was developed and tested based on [Anaconda](https://www.continuum.io/anaconda-overview) 2.4.1.

    The dependencies are:

    1. [GNU Scientific Library (GSL ver. 1.14)](https://www.gnu.org/software/gsl/)
    2. [theano (ver 0.8)](http://deeplearning.net/software/theano/)

    After fork, you need to configure several things before running the code:

    1. Download GSL and Anaconda then install, use pip to install theano;
    2. Enter directory 'cr', modify the Makefile to configure the library path of GSL, and compile;
    3. Create the directories for the upcoming models.

    If you are OK with using the project directory as workspace, please run intialize.sh directly:

        chmod +x initialize.sh
        sh initialize.sh

    Enter directory 'methods' to run the training script of CER and DPM.

    You can use following commands (let me use content 'tfidf' as example):

        python clr_train.py -fp ../contents/tfidf.npy -fn tfidf -wd ../models/cer
        python dpm_train.py -fp ../contents/tfidf.npy -fn tfidf -wd ../models/dpm

    To evaluate CER and DPM, setting the variable 'model_root' (default is '../models/cer') in test.py, then run:

        python test.py

    To train BPR and VBPR, you can use following commands:

        python bpr_train.py
        python vbpr_train.py

    To evaluate BPR and VBPR, you can use following commands:

        python bpr_test.py
        python vbpr_test.py

    To evaluate the fusion methods, you can use following commands:

        python pfusion.py // the proposed fusion method
        python afusion.py // the average fusion method
        python bfusion.py // the bpr based fusion method
        python efusion.py // the error based fusion method, the error is measured by RMSE
        python sfusion.py // the svm based fusion method

* * *
* ## **Dataset**

    Due to the file size limitation, the data for training and testing are maintained by other services.

    The 5-fold experimental data can be downloaded from below link:

    1. [Google Drive](https://drive.google.com/open?id=0Bz6bXb44ws2WcGtyNGltajJTcWc)
    2. [Baidu Yunpan](http://pan.baidu.com/s/1jHPBVgy)

    The content features can be downloaded from below link:

    1. [Google Drive](https://drive.google.com/open?id=0Bz6bXb44ws2WUXBuVGwzNDBlQXM)
    2. [Baidu Yunpan](http://pan.baidu.com/s/1kVuHWnP)

    To make the code run correctly, these extenal data should be put to the project directory.

    The original 10380 videos can be downloaded from below link:

    1. [Baidu Yunpan](http://pan.baidu.com/s/1jIDdAwI)

* * *
* ## **Reference**
    If you use above codes or data, please cite the paper below:

        @article{VCRS,
            author    = {Xingzhong Du and Hongzhi Yin and Ling Chen and Yang Wang and Yi Yang and Xiaofang Zhou},
            title     = {Exploiting Rich Contents for Personalized Video Recommendation},
            journal   = {CoRR},
            volume    = {abs/1612.06935}, 
            year      = {2016}
        }